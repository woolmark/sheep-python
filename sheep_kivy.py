#!/usr/bin/env kivy
# coding: UTF-8

import sys

from kivy.app import App
from kivy.clock import Clock
from kivy.properties import NumericProperty
from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivy.core.image import Image as CoreImage

import pysheep

class SheepWidget(Image):

    def __init__(self, sheep, images):
        super(SheepWidget, self).__init__(allow_stretch = True)

        self.x = -sys.maxsize
        self.y = -sys.maxsize
        self.width = sheep.width
        self.height = sheep.height

        self.__sheep = sheep
        self.__images = images
        self.is_maximal_position = self.__sheep.is_maximal_position
        self.is_go_away = self.__sheep.is_go_away

    def move(self):
        self.__sheep.move()
        if not self.__sheep.stretch:
            self.texture = self.__images[0].texture
        else:
            self.texture = self.__images[1].texture

        parent = self.get_parent_window()
        if parent is not None:
            self.x = self.__sheep.x
            self.y = parent.height - self.__sheep.y - self.__sheep.height

class SheepFrame(Widget):

    ground_width = NumericProperty()
    ground_height = NumericProperty()
    fence_x = NumericProperty()
    fence_width = NumericProperty()
    fence_height = NumericProperty()

    count = NumericProperty(0)

    __flock = []
    __gate_opened = False

    def __init__(self):
        super(SheepFrame, self).__init__()
        self.bind(size=self.on_size_changed)

        self.__frame = pysheep.Frame()
        self.__images = [ CoreImage('sheep00.png'), CoreImage('sheep01.png') ]

        Clock.schedule_interval(self.update, 0.1)

    def on_size_changed(self, window, size):
        self.__frame.set_size(size[0], size[1])
        self.ground_width = self.__frame.ground.width
        self.ground_height = self.__frame.ground.height
        self.fence_x = self.__frame.fence.x
        self.fence_width = self.__frame.fence.width
        self.fence_height = self.__frame.fence.height

        for sheep in self.__flock:
            self.remove_widget(sheep)
        self.__flock = []

    def update(self, delta):

        new_flock = []
        jumped_sheep = 0

        if self.__gate_opened or len(self.__flock) == 0:
            sheep = SheepWidget(pysheep.Sheep(self.__frame), self.__images)
            new_flock.append(sheep)
            self.add_widget(sheep)

        for sheep in self.__flock:
            sheep.move()
            if sheep.is_maximal_position():
                jumped_sheep += 1
            if not sheep.is_go_away():
                new_flock.append(sheep)
            else:
                self.remove_widget(sheep)

        self.__flock = new_flock

        self.count += jumped_sheep

    def on_touch_down(self, event):
        self.__gate_opened = True

    def on_touch_up(self, event):
        self.__gate_opened = False

class SheepApp(App):
    def build(self):
        self.icon = 'icon.png'
        return SheepFrame()

if __name__ == '__main__':
    SheepApp().run()

# vim: ts=4 sw=4 et fe=UTF-8 ff=mac
