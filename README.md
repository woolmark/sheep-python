sheep for Python

# Requirements

 - Python 2.7 or later, Python 3.0 or later
 - (Optional) [pygame]
 - (Optional) [Kivy] version 2 or 3
 - (Optional) [wxPython]

# Development on Mac

## CLI

Run `pysheep.py`.

    % python pysheep.py

## pygame

Install [pygame] with [Homebrew].

    % brew install homebrew/python/pygame

After that, set setup `PYTHONPATH` env to append `pygame` module into `sys.path`. `brew info pygame` help you how to do.

It will install too many packages, ex. gcc, mercurial, stl and so on. If you don't want, you maybe install `pygame` with the other procedure.

Run `sheep_pygame.py` with `python` command.

    % python sheep_pygame.py

## Kivy

Install [Kivy] as Application(.app).

https://kivy.org/docs/installation/installation-osx.html

Run `sheep_kivy.py` with `kivy` command, without `python` or `python3` command.

    % kivy sheep_kivy.py --size 300x200

## wxPython

TODO:

# License

[WTFPL]

[pygame]: http://www.pygame.org/wiki/about
[Kivy]: http://kivy.org
[Homebrew]: http://brew.sh
[WTFPL]: http://www.wtfpl.net "WTFPL"
