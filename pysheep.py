#!/usr/bin/env python
# coding: UTF-8

import sys
import random

TITLE = u"sheep"
SKY_RGB = (150, 150, 255)
GROUND_RGB = (100, 255, 100)

SHEEP_WIDTH = 17
SHEEP_HEIGHT = 12

FENCE_WIDTH = 52
FENCE_HEIGHT = 78

class Rect:
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def __str__(self):
        return u"(%d, %d, %d, %d)" % (self.x, self.y, self.width, self.height)

class Sheep(Rect):
    X_MOVING_DIST = 5
    Y_MOVING_DIST = 3
    JUMP_FRAME = 4

    stretch = False
    scale = 1
    __jump_x = 0
    __jump_state = -sys.maxsize

    def __init__(self, frame):
        Rect.__init__(self, 0, 0, SHEEP_WIDTH * frame.scale, SHEEP_HEIGHT * frame.scale)
        self.x = frame.width
        self.y = frame.height - frame.ground.height + random.randrange(frame.ground.height - self.height)

        # y = -1 * fence.height / fence.width * (x - (width - fence.width) / 2) + height
        self.__jump_x = -1 * (self.y - frame.height) * frame.fence.width / frame.fence.height + (frame.width - frame.fence.width) / 2

        self.scale = frame.scale

    def move(self):
        self.x -= self.X_MOVING_DIST * self.scale

        if self.__jump_state < 0 and self.__jump_x <= self.x and self.x < self.__jump_x + self.X_MOVING_DIST * self.scale:
            self.__jump_state = 0

        if self.__jump_state >= 0:
            if self.__jump_state < self.JUMP_FRAME:
                self.y -= self.Y_MOVING_DIST * self.scale
            elif self.__jump_state < self.JUMP_FRAME * 2:
                self.y += self.Y_MOVING_DIST * self.scale
            else:
                self.__jump_state = -sys.maxsize - 1

            self.__jump_state += 1

            self.stretch = True
        else:
            self.stretch = not self.stretch

    def is_go_away(self):
        return self.x < -self.width

    def is_maximal_position(self):
        return self.__jump_state == self.JUMP_FRAME

    def __str__(self):
        return u"(%3d, %3d) stretch:%r" % (self.x, self.y, self.stretch)

class Frame(Rect):
    DEFAULT_WIDTH = 100
    DEFAULT_HEIGHT = 100

    fence = None
    ground = None
    scale = 1

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        Rect.__init__(self, 0, 0, width, height)
        self.set_size(width, height)

    def set_size(self, width, height):
        self.width = width
        self.height = height
        if width < height:
            self.scale = int(width / self.DEFAULT_WIDTH)
        else:
            self.scale = int(height / self.DEFAULT_HEIGHT)
        if self.scale < 1:
            self.scale = 1

        width = FENCE_WIDTH * self.scale
        height = FENCE_HEIGHT * self.scale
        self.fence = Rect((self.width  - width) / 2, self.height - height, width, height)

        height = int(FENCE_HEIGHT * self.scale * 0.9)
        self.ground = Rect(0, self.height - height, self.width, height)

 
if __name__ == '__main__':
    flock = []
    flock.append(Sheep(Frame()))

    while len(flock) > 0:
        new_flock = []

        for sheep in flock:
            sheep.move()
            print(sheep)
            if sheep.is_maximal_position():
                print("JUMP!!")
            if not sheep.is_go_away():
                new_flock.append(sheep)

        flock = new_flock

# vim: ts=4 sw=4 et fe=UTF-8 ff=mac
