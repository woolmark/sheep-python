#!/usr/bin/env python
# coding: UTF-8

import sys
import pygame
from pygame.locals import *

import pysheep

__counter_font = None

__sheep00_image = None
__sheep01_image = None
__fence_image = None

def create_screen(frame):
    global __counter_font, __sheep00_image, __sheep01_image, __fence_image

    screen = pygame.display.set_mode((frame.width, frame.height), 0, 32)
    pygame.display.set_caption(pysheep.TITLE)
    pygame.display.set_icon(pygame.image.load("icon.png").convert())

    if not __sheep00_image:
        __sheep00_image = pygame.image.load("sheep00.png").convert_alpha()
    if not __sheep01_image:
        __sheep01_image = pygame.image.load("sheep01.png").convert_alpha()
    if not __fence_image:
        __fence_image = pygame.image.load("fence.png").convert_alpha()
    if not __counter_font:
        __counter_font = pygame.font.SysFont(None, 18)

    return screen

def draw_background(screen, frame):
    global __fence_image
    screen.fill(pysheep.SKY_RGB)

    pygame.draw.rect(screen, pysheep.GROUND_RGB, pygame.Rect(frame.ground.x, frame.ground.y, frame.ground.width, frame.ground.height))
    screen.blit(pygame.transform.scale(__fence_image, (frame.fence.width, frame.fence.height)), (frame.fence.x, frame.fence.y))

def draw_sheep(screen, sheep):
    global __sheep00_image, __sheep01_image
    if sheep.stretch:
        screen.blit(pygame.transform.scale(__sheep01_image, (sheep.width, sheep.height)), (sheep.x, sheep.y))
    else:
        screen.blit(pygame.transform.scale(__sheep00_image, (sheep.width, sheep.height)), (sheep.x, sheep.y))

def draw_counter(screen, count):
    global __counter_font

    count = __counter_font.render("%d sheep" % count, False, (0, 0, 0))
    screen.blit(count, (5, 5))

if __name__ == '__main__':
    pygame.init()

    frame = pysheep.Frame(300, 200)
    screen = create_screen(frame)

    count = 0
    gate_opened = False
    flock = []

    clock = pygame.time.Clock()
    while True:
        clock.tick(10)

        draw_background(screen, frame)

        new_flock = []
        for sheep in flock:
            sheep.move()
            draw_sheep(screen, sheep)
            if sheep.is_maximal_position():
                count += 1
            if not sheep.is_go_away():
                new_flock.append(sheep)
        flock = new_flock

        if gate_opened or len(flock) == 0:
            flock.append(pysheep.Sheep(frame))

        draw_counter(screen, count)

        pygame.display.update()

        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit(0)
            elif event.type == MOUSEBUTTONDOWN and event.button == 1:
                gate_opened = True
            elif event.type == MOUSEBUTTONUP and event.button == 1:
                gate_opened = False

# vim: ts=4 sw=4 et fe=UTF-8 ff=mac
